const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    desc: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    category: {
        type: String,
        enum: ['Electronics', 'Gaming', 'Fashion', 'Beauty', 'Food']
    },
    date: {
        type: Date,
        required: true
    },
    image: {
        type: String,
        required: true,
        get: imageUrl
    }
}, 
{
    toJSON: {getters: true}
})

function imageUrl(image) {
    return `${process.env.BASE_URL}/products/image?image=${image}`
}

ProductSchema.index({name: 'text'})

const ProductModel = mongoose.model('Products', ProductSchema)

module.exports = ProductModel

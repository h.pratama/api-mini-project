const mongoose = require('mongoose')
const { Schema } = require('mongoose')

const MessagesSchema = new Schema({
    users: [
        {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        }
    ],
    lastChat: {
        users: [
            {
                type: Schema.Types.ObjectId,
                required: true,
                ref: 'User'
            }
        ],
        sender: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        message: {
            type: String,
            required: true
        },
        createAt: {
            type: Date,
            required: true
        },
        updateAt: {
            type: Date,
            required: true
        }
    },
    createAt: {
        type: Date,
        required: true
    },
    updateAt: {
        type: Date,
        required: true
    }
},
{
    toJSON: { virtuals: true }
})

const MessagesModel = mongoose.model('Messages', MessagesSchema)

module.exports = MessagesModel

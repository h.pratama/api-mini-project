const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const ImageSchema = new Schema({
    url: {
        type: String,
        required: true,
        get: imageUrl
    },
    refId: String
}
,
{
    toJSON: { getters: true }
}
)

function imageUrl(image) {
    let getImage = image
    if (image && String(image).length > 0 && String(image).includes('\\')) {
        const splitImg = image.split('\\')
        getImage = `${splitImg[0]}/${splitImg[1]}/${splitImg[2]}`
    }
    
    return `${process.env.BASE_URL}/upload-image?image=${getImage}`
}

const ImageModel = mongoose.model('Image', ImageSchema)
module.exports = ImageModel

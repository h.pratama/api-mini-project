const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const WalletSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'user data is required'],
        ref: 'User',
        // index: true,
        unique: true,
    },
    amount: {
        type: Number,
        required: [true, 'amount is required'],
        min: 0,
    },
    createdAt: {
        type: Date,
        required: true
    },
    updatedAt: {
        type: Date,
        required: true
    }
})

const WalletModel = mongoose.model('Wallet', WalletSchema)
module.exports = WalletModel

const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const FinanceSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'user data is required'],
        ref: 'User',
    },
    title: {
        type: String,
        required: true
    },
    desc: String,
    amount: {
        type: Number,
        required: [true, 'amount is required'],
        min: 0,
    },
    type: {
        type: String,
        enum: {
            values: ['income', 'outcome'],
            message: '{VALUE} is not supported'
        }
    },
    createdAt: {
        type: Date,
        required: true
    },
    updatedAt: {
        type: Date,
        required: true
    }
})

const FinanceModel = mongoose.model('Finance', FinanceSchema)
module.exports = FinanceModel

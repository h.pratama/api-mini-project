const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const PlaceSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'user data is required'],
        ref: 'User',
    },
    name: {
        type: String,
        required: true
    },
    desc: String,
    isFavorite: {
        type: Boolean,
        set: (favorite) => {
            if (favorite == false || 
                favorite == null ||  
                favorite == undefined || 
                !favorite ) return false 
            else return true
        }
    },
    isDefault: {
        type: Boolean,
        set: (favorite) => {
            if (favorite == false || 
                favorite == null ||  
                favorite == undefined || 
                !favorite ) return false 
            else return true
        }
    },
    createdAt: {
        type: Date,
        required: true
    },
    updatedAt: {
        type: Date,
        required: true
    }
})


const PlaceModel = mongoose.model('Place', PlaceSchema)
module.exports = PlaceModel

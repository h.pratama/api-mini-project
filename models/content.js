const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const ContentSchema = new Schema({
    // user: {
    //     type: Schema.Types.ObjectId,
    //     required: [true, 'user data is required'],
    //     ref: 'User',
    // },
    title: {
        type: String,
        required: true
    },
    desc: String,
    type: {
        type: String,
        required: true,
    },
    video: {
        type: String,
        required: true
    },
    thumbnail: {
        type: String,
        required: true,
        get: imageUrl
    },
    createdAt: {
        type: Date,
        required: true
    },
    updatedAt: {
        type: Date,
        required: true
    }
}
,
{
    toJSON: { getters: true }
})

function imageUrl(image) {
    let getImage = image
    if (image && String(image).length > 0 && String(image).includes('\\')) {
        const splitImg = image.split('\\')
        getImage = `${splitImg[0]}/${splitImg[1]}/${splitImg[2]}`
    }
    
    return `${process.env.BASE_URL}/content/image?image=${getImage}`
}

ContentSchema.index({title: 'text'})
const ContentModel = mongoose.model('Content', ContentSchema)
module.exports = ContentModel

const mongoose = require('mongoose')
const { Schema } = require('mongoose')

const CartsSchema = new Schema({
    user: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    product: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'Products',
        get: productImageUrl
    },
    isCheckOut: {
        required: true,
        type: Boolean
    },
    date: {
        required: true,
        type: Date
    }
}, {
    toJSON: { virtuals: true }
})

function productImageUrl(product) {
    return {
        ...product,
        image: `${process.env.BASE_URL}/products/image?image=${product.image}`
    }
}

const CartsModel = mongoose.model('Carts', CartsSchema)

module.exports = CartsModel

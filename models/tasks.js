const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const TasksSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    desc: {
        type: String,
    },
    status: {
        type: String,
        enum: ['todo', 'progress', 'review', 'done'],
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    createdAt: {
        type: Date,
        required: true
    },
    updatedAt: {
        type: Date,
        required: true
    }
})

const TasksModel = mongoose.model('Tasks', TasksSchema)
module.exports = TasksModel

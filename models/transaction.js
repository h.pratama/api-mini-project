const mongoose = require('mongoose')
const { Schema } = require('mongoose')

const TransactionsSchema = new Schema({
    user: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    products: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Products',
            required: true,
            get: gettersProduct
        }
    ],
    status: {
        required: true,
        enum: ['waiting', 'paid', 'canceled'],
        type: String
    },
    amount: {
        required: true,
        type: Number
    },
    date: {
        required: true,
        type: Date
    },

}, {
    toJSON: { virtuals: true }
})

function gettersProduct(product) {
    return {
        ...product,
        image: `${process.env.BASE_URL}/products/image?image=${product.image}`
    }
}

const TransactionsModel = mongoose.model('Transactions', TransactionsSchema)

module.exports = TransactionsModel

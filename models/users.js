const mongoose = require('mongoose')
const { Schema } = require('mongoose')

const UsersSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
        enum: ['admin', 'user'],
        set: (role) => {
            if (role && role.length > 0) return role
            else return 'user'
        }
    },
    avatar: {
        type: String,
        get: imageUrl
    },
    registerAt: {
        type: Date,
        required: true
    },
    phone: String
}
,
{
    toJSON: { getters: true }
})

function imageUrl(image) {
    let getImage = image ?? 'assets/images/avatar.jpeg'
    
    if (image && String(image).length > 0 && String(image).includes('\\')) {
        const splitImg = image.split('\\')
        getImage = `${splitImg[0]}/${splitImg[1]}/${splitImg[2]}`
    }

    return `${process.env.BASE_URL}/user/profile/image?image=${getImage}`
}

const UsersModel = mongoose.model('User', UsersSchema)

module.exports = UsersModel

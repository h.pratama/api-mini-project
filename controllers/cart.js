const { ObjectId } = require("mongodb")
const CartsModel = require("../models/cart");
const ProductModel = require("../models/products");

class CartController {
    static async getCarts(req, res) {
        try {
            const carts = await CartsModel
                .find({
                    user: ObjectId(req.user.id),
                    isCheckOut: false
                })
                .populate('product')
            res.status(200).json({status: 200, ok: true, data: carts})
        } catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error.message) });
        }
    }

    static async postCart(req, res) {
        try {
            const { productId } = req.body

            const getProduct = await ProductModel.findById(productId)

            if (getProduct && '_id' in getProduct) {
                const cart = {
                    user: req.user.id,
                    product: productId,
                    date: new Date(),
                    isCheckOut: false,
                }

                const insertCart = new CartsModel(cart)
                await insertCart.save()
                res.status(200).json({ status: 200, ok: true, message: 'Success add cart' });
            } else {
                req.status(200).json({status: 404, message: 'Product not found'})
            }
            
        } catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error) });
            
        }
    }

    static async deleteCart(req, res) {
		try {
            const getCart = await CartsModel.findOne({
                _id: ObjectId(req.params.id),
                user: ObjectId(req.user.id)
            })

            if (getCart && '_id' in getCart) {
                await CartsModel.deleteOne({ 
                    _id: ObjectId(req.params.id),
                    user: ObjectId(req.user.id)
                });
                res.status(200).json({ status: 200, ok: true, message: 'Success delete cart' });
            } else {
                res.status(200).json({ status: 404, ok: true, message: 'Data not found' });
            }
		} catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: error.message });
		}
	}

}

module.exports = CartController

const path = require('path')
const { ObjectId } = require('mongodb')

const { comparePassword, hash } = require("../helpers/bcrypt")
const { generateToken } = require("../helpers/jwt")
const UsersModel = require("../models/users")


class UserController {
    static async login(req, res) {
        try {
            const findUser = await UsersModel.findOne({ email: req.body.email })
            if (findUser && comparePassword(req.body.password, findUser.password)) {
                const payload = {
                    _id: findUser._id,
                    name: findUser.name,
                    email: findUser.email,
                    role: findUser.role
                }

                res.status(200).json({ status: 200, message: 'success', token: generateToken(payload), role: findUser.role, user: payload })
            } else {
                res.status(200).json({ status: 200, message: 'Not found' })
            }
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Oops!! something error' })
        }
    }

    static async register(req, res) {
        try {
            const findUser = await UsersModel.findOne({ email: req.body.email })
            if (findUser) {
                res.status(200).json({ status: 200, message: 'User already registered' })
            } else {
                const { name, email, password, role, phone } = req.body
                const newUser = {
                    name: name,
                    email: email,
                    password: hash(password),
                    role: role,
                    registerAt: new Date(),
                    phone,
                }

                const insertUser = new UsersModel(newUser)
                await insertUser.save()

                const {_id, id} = insertUser
                const payload = {
                    _id,
                    id,
                    name: insertUser.name,
                    email: insertUser.email,
                    role: insertUser.role,
                }

                res.status(200).json({ status: 200, token: generateToken(payload), message: 'Success register' })
            }
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Oops!! something error' })
        }
    }

    static async getUsers(req, res) {
        try {
            const getUsers = await UsersModel.find().select({password: 0})

            res.status(200).json({status: 200, data: getUsers})
        } catch (error) {
            console.log(error);
            res.status(500).json({ status: 500, message: 'Oops!! something error' })
        }
    }

    static async getProfile(req, res) {
        try {
            const getUser = await UsersModel.findById(req.user.id).select({password: 0})

            res.status(200).json({status: 200, data: getUser})
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Oops!! something error' })
        }
    }

    static getProfileImage(req, res) {
        try {
            let dirName = __dirname
            if (__dirname.includes('/controllers')) {
                dirName = __dirname.replace('/controllers', '')
            }

            if (__dirname.includes('\controllers')) {
                dirName = __dirname.replace('\controllers', '')
            }

            res.sendFile(path.join(dirName, req.query.image));
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error) });
        }
    }

    static async updateProfile(req, res) {
        try {
            const { name, email } = req.body
            const getUser = await UsersModel.findOneAndUpdate(
                {
                    _id: ObjectId(req.user.id)
                },
                {
                    $set: {
                        name: name,
                        email: email,
                    }
                }
            )
            if (getUser) {
                res.status(200).json({status: 200, message: 'Success update profile'})
            } else {
                res.status(200).json({status: 404, message: 'Data not found'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async updateProfileImage(req, res) {
        try {
            const getUser = await UsersModel.findOneAndUpdate(
                {
                    _id: ObjectId(req.user.id)
                },
                {
                    $set: {
                        avatar: req.files[0].path
                    }
                }
            )
            if (getUser) {
                res.status(200).json({status: 200, message: 'Success update profile'})
            } else {
                res.status(200).json({status: 404, message: 'Data not found'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

}

module.exports = UserController


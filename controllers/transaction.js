const { ObjectId } = require("mongodb");
const CartsModel = require("../models/cart");
const TransactionsModel = require("../models/transaction")

class TransactionController {
    static async getTransactions(req, res) {
        try {
            const transactions = await TransactionsModel
            .find({
                user: ObjectId(req.user.id),
            })
            .populate('products')
            .sort({
                date: -1
            })
            res.status(200).json({status: 200, ok: true, data: transactions})
        } catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error.message) });
        }
    }

    static async getTransaction(req, res) {
        try {
            const transaction = await TransactionsModel
            .findOne({
                _id: ObjectId(req.params.id),
                user: ObjectId(req.user.id)
            })
            .populate('products')
            .sort({
                date: -1
            })
            res.status(200).json({status: 200, ok: true, data: transaction})
        } catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error.message) });
        }
    }

    static async postTransactions(req, res) {
        try {
            const newTransaction = {
                user: req.user.id,
                products: req.body.products,
                status: 'waiting',
                amount: req.body.amount,
                date: new Date()
            }

            const insertTransaction = new TransactionsModel(newTransaction)
            await insertTransaction.save()
            await CartsModel.updateMany(
                { user: req.user.id },
                { $set: { isCheckOut: true } }
            )
            res.status(200).json({ status: 200, ok: true, message: 'Success create transaction', data: insertTransaction });
        } catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error.message) });
        }
    }

    static async updateTransaction(req, res) {
        try {
            let getTransaction = await TransactionsModel.findById(req.params.id)
            
            if (getTransaction && '_id' in getTransaction) {
                const {status} = req.body
    
                const transaction = {
                    status
                }

                getTransaction =  await TransactionsModel.findOneAndUpdate(
                    { _id: ObjectId(req.params.id) },
                    {$set: transaction}
                ).populate('products')

                getTransaction['status'] = status

				res.status(200).json({ status: 200, ok: true, message: 'Success update transaction', data: getTransaction });
            }
        } catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error.message) });
            
        }
    }

    static async deleteTransaction(req, res) {
		try {
			await TransactionsModel.deleteOne({ _id: ObjectId(req.params.id) });
			res.status(200).json({ status: 200, ok: true, message: 'Success delete transaction' });
		} catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: error.message });
		}
	}

}

module.exports = TransactionController

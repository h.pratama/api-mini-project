const { ObjectId } = require('mongodb');
const fs = require('fs');
const path = require('path')

const ProductModel = require("../models/products")

class ProductController {
    
    static async getProducts(req, res) {
        try {
            const products = await ProductModel
                .find()
                .sort({
                    date: -1
                })
                .limit(50);
            
            if (products && products.length && products.length > 0) {

                res.status(200).json({status: 200, ok: true, data: products})
            } else {
                res.status(200).json({status: 200, ok: true, data: []})
            }
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error.message) });
        }
    }

    static async getDetailProduct(req, res) {
		try {
			const product = await ProductModel.findOne({ _id: ObjectId(req.params.id) })

			res.status(200).json({ status: 200, ok: true, data: product });
		} catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error' });
		}
	}

    static getImageProduct(req, res) {
        try {
            const dirName = __dirname.replace('/controllers', '')

            res.sendFile(path.join(dirName, req.query.image));
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error) });
        }
    }

    static async postProduct(req, res) {
		try {
			const body = req.body;

			const product = {
				name: body.name,
				desc: body.desc,
                price: body.price,
				category: body.category,
				date: new Date(),
				image: req.files[0].path
			};

			const insertProduct = new ProductModel(product);
			await insertProduct.save();
			res.status(200).json({ status: 200, ok: true, message: 'Success add product' });
		} catch (error) {
            console.log(error);
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error) });
		}
	}

    static async updateProduct(req, res) {
		try {
			const body = req.body;

			const getProduct = await ProductModel.findOne({ _id: ObjectId(req.params.id) });

			if (getProduct) {
				const product = {
					name: body.name ?? getProduct.name,
					desc: body.desc ?? getProduct.desc,
					category: body.category ?? getProduct.category,
					date: getProduct.date ?? new Date(),
					image: getProduct.image
				};

				await ProductModel.updateOne(
					{ _id: ObjectId(req.params.id) },
					{ $set: product }
				);

				res.status(200).json({ status: 200, ok: true, message: 'Success update product', data: product });
			} else {
				res.status(400).json({ status: 400, ok: true, message: 'Data not found' });
			}

		} catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: error.message });
		}
	}

    static async deleteProduct(req, res) {
		try {
			await ProductModel.deleteOne({ _id: ObjectId(req.params.id) });
			res.status(200).json({ status: 200, ok: true, message: 'Success delete product' });
		} catch (error) {
			res.status(500).json({ status: 500, message: 'Ooops! something error', error: error.message });
		}
	}

}

module.exports = ProductController

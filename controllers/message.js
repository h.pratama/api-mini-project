const {ObjectId} = require('mongodb')

const MessagesModel = require("../models/messages")

class MessageController {
    static async getMessages(req, res) {
        try {
            const getMessages = await MessagesModel.find(
                {
                    users: req.query.userId
                }
            )
            .select({ 'lastChat.users': 0 })
            .populate('users', { password: 0 })

            res.status(200).json({status: 200, data: getMessages})
        } catch (error) {
            console.log(error);
            res.status(400).json({message: 'Error'})
        }
    }

    static async deleteMessage(req, res) {
        try {
            const getMessage = await MessagesModel.findOneAndDelete(
                {_id: ObjectId(req.params.id)}
            )
            if (getMessage && '_id' in getMessage) {
                res.status(200).json({status:200, message: 'Success delete'})
            } else {
                res.status(200).json({status:200, message: 'Data not found'})
            }
        } catch (error) {
            res.status(400).json({message: 'Error'})
        }
    }
}

module.exports = MessageController

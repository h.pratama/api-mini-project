const {ObjectId} = require('mongodb')
const FinanceModel = require("../models/finance")

class FinanceController {
    constructor() {

    }

    static async getFinances(req, res) {
        try {
            const getFinances = await FinanceModel.find(
                {
                    user: ObjectId(req.user.id)
                }
            ).sort({createdAt: -1})


            res.status(200).json({status: 200, data: getFinances})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async getFinance(req, res) {
        try {
            const getFinance = await FinanceModel.findById(req.params.id)

            res.status(200).json({status: 200, data: getFinance})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async postFinance(req, res) {
        try {
            const { title, desc, amount, type } = req.body
            const finance = {
                user: req.user.id,
                title,
                desc,
                amount,
                type,
                createdAt: new Date(),
                updatedAt: new Date()
            }

            const insert = new FinanceModel(finance)
            await insert.save()

            res.status(200).json({status: 200, message: 'Success add ' + type})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async updateFinance(req, res) {
        try {
            const { title, desc, amount, type } = req.body
            const finance = {
                title,
                desc,
                amount,
                type,
                updateAt: new Date()
            }

            const getFinance = await FinanceModel.findOneAndUpdate(
                {
                    _id: ObjectId(req.params.id),
                    user: ObjectId(req.user.id)
                },
                {
                    $set: finance
                }
            ) 

            if (!getFinance) {
                res.status(200).json({status: 404, message: 'Data not found'})
            } else {
                res.status(200).json({status: 200, message: 'Success update'})
            }

        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async deleteFinance(req, res) {
        try {
            const getFinance = await FinanceModel.findOneAndDelete(
                {
                    _id: ObjectId(req.params.id),
                    user: ObjectId(req.user.id)
                }
            )

            if (getFinance && '_id' in getFinance) {
                res.status(200).json({status: 200, message: 'Success delete'})
            } else {
                res.status(200).json({status: 404, message: 'Data not found'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }
}

module.exports = FinanceController

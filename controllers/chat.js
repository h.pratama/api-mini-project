const { ObjectId } = require('mongodb')

const ChatModel = require("../models/chats")
const MessagesModel = require("../models/messages")

class ChatController {
    static async getChats(req, res) {
        const {sender, receiver} = req.query

        try {
            const getChats = await ChatModel.find(
                {
                    users: {$all: [sender, receiver]}
                }
            )
            .populate('users', { password: 0 })

            res.status(200).json({status: 200, data: getChats})
        } catch (error) {
            res.status(400).json({message: 'Error'})
        }
    }

    static async postChat(req, res) {
        try {
            const {sender, receiver, message} = req.body

            const getChats = await ChatModel.find(
                {
                    users: {$all: [sender, receiver]}
                }
            )

            const newChat = {
                users: [sender, receiver],
                sender,
                message: message,
                createAt: new Date(),
                updateAt: new Date(),
            }

            const newMessage = {
                users: [sender, receiver],
                lastChat: newChat,
                createAt: new Date(),
                updateAt: new Date(),
            }

            if (!getChats || !getChats.length) {

                const insertMessage = new MessagesModel(newMessage)
                await insertMessage.save()
            }

            if (getChats.length > 0) {
                await MessagesModel.findOneAndUpdate(
                    { user: [sender, receiver] },
                    { $set: { lastChat: newChat } }
                )
            }

            const insert = new ChatModel(newChat)
            await insert.save()

            res.status(200).json({status: 200, message: 'Success'})
        } catch (error) {
            console.log(error);
            res.status(400).json({message: 'Error'})
        }
    }

    static async deleteChat(req, res) {
        try {
            const deleteChat = await ChatModel.findOneAndDelete(
                {_id: ObjectId(req.params.id)}
            )

            if (deleteChat && '_id' in deleteChat) {
                res.status(200).json({status: 200, message: 'Success delete chat'})
            } else {
                res.status(200).json({status: 200, message: 'Data not found'})
            }
        } catch (error) {
            res.status(400).json({message: 'Error'})
        }
    }
}

module.exports = ChatController

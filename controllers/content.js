const path = require('path')
const { ObjectId } = require('mongodb')

const ContentModel = require("../models/content")

class ContentController {
    static async getContents(req, res) {
        try {
            const { type, search } = req.query

            let getContents;

            getContents = await ContentModel.find().sort({createdAt: -1})

            if (type && type.length > 0) {
                getContents = await ContentModel
                .find(
                    {
                        type: type
                    }
                )
                .sort({createdAt: -1})
            }

            if (search && search.length > 0) {
                getContents = await ContentModel
                .find(
                    {
                        $text: { $search: search }
                    }
                )
                .sort({createdAt: -1})
            }

            res.status(200).json({status: 200, data: getContents})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async getContent(req, res) {
        try {
            const getContent = await ContentModel.findById(req.params.id)

            res.status(200).json({status: 200, data: getContent})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static getThumbnailContent(req, res) {
        try {
            let dirName = __dirname
            if (__dirname.includes('/controllers')) {
                dirName = __dirname.replace('/controllers', '')
            }

            if (__dirname.includes('\controllers')) {
                dirName = __dirname.replace('\controllers', '')
            }
            res.sendFile(path.join(dirName, req.query.image));
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error) });
        }
    }

    static async postContent(req, res) {
        try {
            const { title, desc, type, video } = req.body
            const newContent = {
                title,
                desc, 
                type,
                video,
                thumbnail: req.files[0].path,
                createdAt: new Date(),
                updatedAt: new Date()
            }

            if (req.user.role === 'admin') {
                const insert = new ContentModel(newContent)
                await insert.save()
    
                res.status(200).json({status:200, message: 'Success create content'})
            } else {
                res.status(200).json({status:200, message: 'Unautorizhed you do not have access to create data'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async updateThumbnail(req, res) {
        try {
            const { title, desc, type, video } = req.body
            const newContent = {
                title,
                desc, 
                type,
                video,
                thumbnail: req.files[0].path,
                updatedAt: new Date()
            }

            if (req.user.role === 'admin') {
                const getContent = await ContentModel.findOneAndUpdate(
                    {
                        _id: ObjectId(req.params.id),
                    },
                    {
                        $set: newContent
                    }
                )
    
                if (getContent && '_id' in getContent) res.status(200).json({status:200, message: 'Success update content'})
                else {
                    if (getContent && '_id' in getContent) res.status(200).json({status:404, message: 'Data not found'})
                }
            } else {
                res.status(200).json({status:200, message: 'Unautorizhed you do not have access to create data'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async updateContent(req, res) {
        try {
            const { title, desc, type, video } = req.body
            const newContent = {
                title,
                desc, 
                type,
                video,
                updatedAt: new Date()
            }

            if (req.user.role === 'admin') {
                const getContent = await ContentModel.findOneAndUpdate(
                    {
                        _id: ObjectId(req.params.id),
                    },
                    {
                        $set: newContent
                    }
                )
    
                if (getContent && '_id' in getContent) res.status(200).json({status:200, message: 'Success update content'})
                else {
                    if (getContent && '_id' in getContent) res.status(200).json({status:404, message: 'Data not found'})
                }
            } else {
                res.status(200).json({status:200, message: 'Unautorizhed you do not have access to create data'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async deleteContent(req, res) {
        try {
            if (req.user.role === 'admin') {
                const getContent = await ContentModel.findByIdAndDelete(req.params.id)
    

                if (getContent) res.status(200).json({status: 200, message: 'Success delete'})
                else res.status(200).json({status: 404, message: 'Data not found'})
            } else {
                res.status(200).json({status:200, message: 'Unautorizhed you do not have access to create data'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
            
        }
    }
}

module.exports = ContentController

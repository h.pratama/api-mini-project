const path = require('path')
const { ObjectId } = require('mongodb')
const ImageModel = require('../models/uploadImg')

class UploadImgController {
    static async getImg(req, res) {
        try {
            const getImg = await ImageModel.findOne({
                refId: req.params.id
            })
            
            res.status(200).json({status: 200, data: getImg})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static getUrlImge(req, res) {
        try {
            let dirName = __dirname
            if (__dirname.includes('/controllers')) {
                dirName = __dirname.replace('/controllers', '')
            }

            if (__dirname.includes('\controllers')) {
                dirName = __dirname.replace('\controllers', '')
            }


            res.sendFile(path.join(dirName, req.query.image));
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error) });
        }
    }

    static async postImg(req, res) {
        try {
            const { refId } = req.body
            const newContent = {
                refId,
                url: req.files[0].path,
            }

            const insert = new ImageModel(newContent)
            await insert.save()
            res.status(200).json({status:200, message: 'Success upload image', data: insert})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }
}

module.exports = UploadImgController

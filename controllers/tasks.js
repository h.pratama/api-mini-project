const { ObjectId } = require('mongodb')
const TasksModel = require("../models/tasks")


const TaskController = {
    getTasks: async (req, res) => {
        try {
            const tasks = await TasksModel.find({
                user: ObjectId(req.user.id)
            })
            .sort({updatedAt: -1})

            res.status(200).json({status: 200, data: tasks})
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error.message) });
        }
    },
    getTask: async (req, res) => {
        try {
            const task = await TasksModel.findById(req.params.id)

            res.status(200).json({status: 200, data: task})
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error: JSON.stringify(error.message) });
        }
    },

    postTask: async (req, res) => {
        try {
            const {title, desc, status} = req.body

            const data = {
                title,
                desc,
                status,
                user: req.user.id,
                createdAt: new Date(),
                updatedAt: new Date()
            }

            const insertTask = new TasksModel(data)
            await insertTask.save()
            res.status(201).json({status: 201, message: 'Success add task'})
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error });
        }
    },

    updateTask: async (req, res) => {
        try {
            const getTask = await TasksModel.findOne(
                {
                    _id: ObjectId(req.params.id),
                    user: ObjectId(req.user.id)
                }
            )

            if (getTask && '_id' in getTask) {
                const { title, desc } = getTask
                const update = {
                    title,
                    desc,
                    status: req.body.status,
                    updatedAt: new Date()
                }

                await TasksModel.updateOne(
                    { _id: ObjectId(req.params.id) },
                    { $set: update }
                )

                res.status(200).json({ status: 200, ok: true, message: 'Success update task' });
            } else {
                res.status(200).json({ status: 404, ok: true, message: 'Data not found' });
            }
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error });
        }
    },

    deleteTask: async (req, res) => {
        try {
            const getTask = await TasksModel.findOneAndDelete(
                {
                    _id: ObjectId(req.params.id),
                    user: ObjectId(req.user.id)
                }
            )

            if (getTask) {
                res.status(200).json({status: 200, message: 'Success delete task'})
            } else {
                res.status(200).json({ status: 404, ok: true, message: 'Data not found' });
            }
        } catch (error) {
            res.status(500).json({ status: 500, message: 'Ooops! something error', error: error.message });
        }
    }
}

module.exports = TaskController

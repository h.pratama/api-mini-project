const {ObjectId} = require('mongodb')
const WalletModel = require("../models/wallet")


class WalletController {
    static async getWallet(req, res) {
        try {
            const getWallet = await WalletModel.findOne(
                { user: ObjectId(req.user.id) }
            )

            if (getWallet && 'amount' in getWallet) {
                res.status(200).json({status: 200, message: 'Success', data: getWallet})
            } else {
                res.status(200).json({status: 404, message: 'Data not found'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async postWallet(req, res) {
        try {
            const getWallet = await WalletModel.findOne({
                user: ObjectId(req.user.id)
            })

            if (getWallet && 'amount' in getWallet) {
                const updateWallet = {
                    amount: getWallet.amount + +req.body.amount,
                    updatedAt: new Date()
                }

                await WalletModel.updateOne(
                    { 
                        _id: ObjectId(getWallet._id),
                        user: ObjectId(req.user.id)          
                    },
                    {
                        $set: updateWallet
                    }
                )
            } else {
                const wallet = {
                    user: req.user.id,
                    amount: +req.body.amount,
                    createdAt: new Date(),
                    updatedAt: new Date()
                }
    
                const insert = new WalletModel(wallet)
                await insert.save()
            }

            res.status(200).json({status:200, message:'Success topup wallet'})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async updateWallet(req, res) {
        try {
            const getWallet = await WalletModel.findOne({
                user: ObjectId(req.user.id)
            })

            if (getWallet && 'amount' in getWallet) {
                const { income, outcome } = req.body

                let amount = +getWallet.amount

                if (income > 0) amount += +income
                if (outcome > 0 && amount >= outcome) amount -= +outcome

                const updateWallet = {
                    amount,
                    updatedAt: new Date()
                }

                const getMyWallet = await WalletModel.findOneAndUpdate(
                    { 
                        _id: ObjectId(getWallet._id),
                        user: ObjectId(req.user.id)          
                    },
                    {
                        $set: updateWallet
                    }
                )

                const {user, createdAt, updatedAt} = getMyWallet

                res.status(200).json({
                    status:200, 
                    message:'Success update wallet', 
                    data: { 
                        amount,
                        user,
                        createdAt,
                        updatedAt
                    }
                })
            } else {
                res.status(200).json({status: 404, message: 'Data not found'})
            }

        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }
}

module.exports = WalletController

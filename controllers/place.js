const {ObjectId} = require('mongodb')

const PlaceModel = require("../models/place")

class PlaceController {
    static async getPlace(req, res) {
        try {
            const getPlace = await PlaceModel.findOne(
                { 
                    user: ObjectId(req.user.id),
                    isDefault: true
                }
            )

            res.status(200).json({status: 200, data: getPlace})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async getAllPlace(req, res) {
        try {
            const getPlaces = await PlaceModel.find(
                { 
                    user: ObjectId(req.user.id),
                }
            )

            res.status(200).json({status: 200, data: getPlaces})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async getFavoritePlace(req, res) {
        try {
            const getPlaces = await PlaceModel.find(
                { 
                    user: ObjectId(req.user.id),
                    isFavorite: true
                }
            )

            res.status(200).json({status: 200, data: getPlaces})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async postPlace(req, res) {
        try {
            const getDefaultPlace = await PlaceModel.findOne(
                { 
                    user: ObjectId(req.user.id),
                    isDefault: true
                }
            )

            const getPlace = {
                user: req.user.id,
                name: req.body.name,
                desc: req.body.desc,
                createdAt: new Date(),
                updatedAt: new Date(),
                isDefault: getDefaultPlace ? false : true
            }

            const insert = new PlaceModel(getPlace)
            await insert.save()
            res.status(200).json({status: 200, message: 'Success add place', data: getPlace})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async postToFavoritePlace(req, res) {
        try {
            const getPlace = {
                user: req.user.id,
                name: req.body.name,
                desc: req.body.desc,
                createdAt: new Date(),
                updatedAt: new Date(),
                isFavorite: Boolean(req.body.isFavorite) ?? false
            }

            const insert = new PlaceModel(getPlace)
            await insert.save()
            res.status(200).json({status: 200, message: 'Success add to favorite place', data: getPlace})
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async updatePlace(req, res) {
        try {
            const getPlace = await PlaceModel.findOne(
                { 
                    user: ObjectId(req.user.id),
                    _id: ObjectId(req.params.id),
                }
            )

            if (getPlace) {
                const place = {
                    name: req.body.name ?? getPlace.name,
                    desc: req.body.desc ?? getPlace.desc,
                    updatedAt: new Date(),
                }
    
                await PlaceModel.updateOne(
                    {
                        _id: ObjectId(req.params.id),
                        user: ObjectId(req.user.id)
                    },
                    {
                        $set: place
                    }
                )

                res.status(200).json({status: 200, message: 'Success update place', data: place})
            } else {
                res.status(200).json({status: 404, message: 'Data not found'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async updateFavouritePlace(req, res) {
        const place = {
            isFavorite: req.body.isFavorite == 'false' || !req.body.isFavorite ? false : true
        }

        try {
            let insert = await PlaceModel.findOneAndUpdate(
                {
                    _id: ObjectId(req.params.id),
                    user: ObjectId(req.user.id)
                },
                {
                    $set: place
                }
            )
            if (insert) {
                insert.isFavorite = place.isFavorite
                res.status(200).json({status: 200, message: 'Success update place', data: insert})
            } else {
                res.status(200).json({status: 404, message: 'Data not found'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async updateDefaultPlace(req, res) {
        try {
            const getDefaultPlace = await PlaceModel.find(
                { 
                    user: ObjectId(req.user.id),
                    isDefault: true
                }
            )

            if (req.body.isDefault == 'true' && getDefaultPlace && getDefaultPlace.length > 0) {
                await PlaceModel.updateMany(
                    {
                        user: ObjectId(req.user.id)
                    },
                    {
                        $set: { isDefault: false }
                    }
                )
            }


            const place = {
                isDefault: req.body.isDefault == 'false' || !req.body.isDefault ? false : true
            }

            let insert = await PlaceModel.findOneAndUpdate(
                {
                    _id: ObjectId(req.params.id),
                    user: ObjectId(req.user.id)
                },
                {
                    $set: place
                }
            )


            if (insert) {
                insert['isDefault'] = req.body.isDefault
                res.status(200).json({status: 200, message: 'Success update place', data: insert})
            } else {
                res.status(200).json({status: 404, message: 'Data not found'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

    static async deletePlace(req, res) {
        try {

            const deletePlace = await PlaceModel.findOneAndDelete(
                {
                    _id: ObjectId(req.params.id),
                    user: ObjectId(req.user.id)
                }
            )
            if (deletePlace) {
                res.status(200).json({status: 200, message: 'Success delete place', data: deletePlace})
            } else {
                res.status(200).json({status: 404, message: 'Data not found'})
            }
        } catch (error) {
            if (error.errors) {
                res.status(400).json({status: 400, message: error.errors})
            } else if (error.message) {
                res.status(400).json({status: 400, message: error.message})
            } else {
                res.status(400).json({status: 400, message: 'Something Error'})
            }
        }
    }

}

module.exports = PlaceController

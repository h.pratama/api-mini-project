require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path')

const app = express();
const router = require('./routers');
const PORT = process.env.PORT || 3001;

mongoose.connect(process.env.DATABASE, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('error', err => {
	console.log('connection error:', err.message);
});
db.once('open', () => {
	console.log('database connected');
});

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.use(router);

app.listen(PORT, () => {
	console.log(`app running on port ${ PORT }`);
});

app.get("/", (req, res) => {
	res.send('Hallo')
});

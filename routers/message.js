const MessageController = require('../controllers/message')

const router = require('express').Router()

router.get('/', MessageController.getMessages)
router.delete('/:id', MessageController.deleteMessage)

module.exports = router

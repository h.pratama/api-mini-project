const ProductController = require('../controllers/product');
const { uploadImage } = require('../middlewares/multer');

const router = require('express').Router()

router.get('/', ProductController.getProducts);
router.post('/', uploadImage, ProductController.postProduct)
router.get('/image', ProductController.getImageProduct)
router.get('/:id', ProductController.getDetailProduct)
router.put('/:id', ProductController.updateProduct)
router.delete('/:id', ProductController.deleteProduct)

module.exports = router

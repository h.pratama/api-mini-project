const router = require('express').Router()

const UserController = require('../controllers/users')
const { authenticaton } = require('../middlewares/authentication')
const { uploadImage } = require('../middlewares/multer')

router.get('/', UserController.getUsers)
router.post('/login', UserController.login)
router.post('/register', UserController.register)
router.get('/profile/image', UserController.getProfileImage)
router.use(authenticaton)
router.get('/profile', UserController.getProfile)
router.put('/profile', UserController.updateProfile)
router.put('/profile/image', uploadImage, UserController.updateProfileImage)

module.exports = router

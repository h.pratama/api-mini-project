const router = require('express').Router()

const CartController = require('../controllers/cart')
const { authenticaton } = require('../middlewares/authentication')

router.use(authenticaton)
router.get('/', CartController.getCarts)
router.post('/', CartController.postCart)
router.delete('/:id', CartController.deleteCart)

module.exports = router

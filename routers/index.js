const router = require('express').Router()

const userRouter = require('./users')
const newsRouter = require('./news')
const imagesRouter = require('./images')
const documentsRouter = require('./documents')
const articleRouter = require('./article')
const videoRouter = require('./videos')
const kambioRouter = require('./kambio')
const wofRouter = require('./wof')
const taskRouter = require('./task')
const productRouter = require('./product')
const cartRouter = require('./cart')
const transactionRouter = require('./transaction')
const messageRouter = require('./message')
const chatRouter = require('./chat')
const walletRouter = require('./wallet')
const financeRouter = require('./finance')
const contentRouter = require('./content')
const placeRouter = require('./place')
const imgRouter = require('./image')

router.use('/user', userRouter)
router.use('/news', newsRouter)
router.use('/articles', articleRouter)
router.use('/images', imagesRouter)
router.use('/file', documentsRouter)
router.use('/videos', videoRouter)
router.use('/kambio', kambioRouter)
router.use('/wof', wofRouter)
router.use('/task', taskRouter)
router.use('/products', productRouter)
router.use('/carts', cartRouter)
router.use('/transactions', transactionRouter)
router.use('/message', messageRouter)
router.use('/chat', chatRouter)
router.use('/wallet', walletRouter)
router.use('/finance', financeRouter)
router.use('/content', contentRouter)
router.use('/place', placeRouter)
router.use('/upload-image', imgRouter)

module.exports = router

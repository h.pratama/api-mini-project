const placeRouter = require('express').Router()

const PlaceController = require('../controllers/place')
const { authenticaton } = require('../middlewares/authentication')

placeRouter.use(authenticaton)
placeRouter.get('/', PlaceController.getPlace)
placeRouter.post('/', PlaceController.postPlace)

placeRouter.get('/all', PlaceController.getAllPlace)
placeRouter.get('/favorite', PlaceController.getFavoritePlace)
placeRouter.post('/favorite', PlaceController.postToFavoritePlace)
placeRouter.put('/favorite/:id', PlaceController.updateFavouritePlace)
placeRouter.put('/default/:id', PlaceController.updateDefaultPlace)

placeRouter.put('/:id', PlaceController.updatePlace)
placeRouter.delete('/:id', PlaceController.deletePlace)

module.exports = placeRouter

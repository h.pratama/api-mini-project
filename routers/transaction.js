const router = require('express').Router()

const TransactionController = require('../controllers/transaction')
const { authenticaton } = require('../middlewares/authentication')

router.use(authenticaton)
router.get('/', TransactionController.getTransactions)
router.post('/', TransactionController.postTransactions)
router.get('/:id', TransactionController.getTransaction)
router.put('/:id', TransactionController.updateTransaction)
router.delete('/:id', TransactionController.deleteTransaction)

module.exports = router;

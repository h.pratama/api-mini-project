const taskRouter = require('express').Router()

const TaskController = require('../controllers/tasks')
const { authenticaton } = require('../middlewares/authentication')

taskRouter.use(authenticaton)
taskRouter.get('/', TaskController.getTasks)
taskRouter.post('/', TaskController.postTask)
taskRouter.get('/:id', TaskController.getTask)
taskRouter.put('/:id', TaskController.updateTask)
taskRouter.delete('/:id', TaskController.deleteTask)

module.exports = taskRouter

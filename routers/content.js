const ContentController = require('../controllers/content')
const { authenticaton } = require('../middlewares/authentication')
const { uploadImage } = require('../middlewares/multer')

const contentRouter = require('express').Router()

contentRouter.get('/', ContentController.getContents)
contentRouter.get('/image', ContentController.getThumbnailContent)
contentRouter.get('/:id', ContentController.getContent)
contentRouter.use(authenticaton)
contentRouter.post('/', uploadImage, ContentController.postContent)
contentRouter.put('/:id', ContentController.updateContent)
contentRouter.put('/update-with-thumbnail/:id', uploadImage, ContentController.updateThumbnail)
contentRouter.delete('/:id', ContentController.deleteContent)

module.exports = contentRouter

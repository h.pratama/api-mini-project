const ChatController = require('../controllers/chat')

const router = require('express').Router()

router.get('/', ChatController.getChats)
router.post('/', ChatController.postChat)
router.delete('/:id', ChatController.deleteChat)

module.exports = router

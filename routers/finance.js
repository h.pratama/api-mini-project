const financeRouter = require('express').Router()

const FinanceController = require('../controllers/finance')
const { authenticaton } = require('../middlewares/authentication')

financeRouter.use(authenticaton)
financeRouter.get('/', FinanceController.getFinances)
financeRouter.post('/', FinanceController.postFinance)
financeRouter.get('/:id', FinanceController.getFinance)
financeRouter.put('/:id', FinanceController.updateFinance)
financeRouter.delete('/:id', FinanceController.deleteFinance)

module.exports = financeRouter

const UploadImgController = require('../controllers/uploadImg')
const { uploadImage } = require('../middlewares/multer')

const imgRouter = require('express').Router()

imgRouter.get('/', UploadImgController.getUrlImge)
imgRouter.get('/:id', UploadImgController.getImg)
imgRouter.post('/', uploadImage, UploadImgController.postImg)

module.exports = imgRouter;

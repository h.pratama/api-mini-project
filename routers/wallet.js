const WalletController = require('../controllers/wallet')
const { authenticaton } = require('../middlewares/authentication')

const walletRouter = require('express').Router()

walletRouter.use(authenticaton)
walletRouter.get('/', WalletController.getWallet)
walletRouter.post('/topup', WalletController.postWallet)
walletRouter.put('/', WalletController.updateWallet)

module.exports = walletRouter

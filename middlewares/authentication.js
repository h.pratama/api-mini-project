const { verifyToken } = require("../helpers/jwt");
const UsersModel = require("../models/users");

const authenticaton = async (req, res, next) => {
    try {
        if (req.headers.authorization) {
            const token = req.headers.authorization.replace('Bearer ', '')
            const decode = verifyToken(token)

            // console.log(Math.floor(Date.now()), '<< Date.now()');
            // console.log(decode.exp, '<< decode.exp');

            // if (Date.now() > decode.exp) {
            //     throw 'Token expaired'
            // } else 

            if ('_id' in decode) {
                const getUser = await UsersModel.findById(decode._id)

                if ('_id' in getUser) {
                    req.user = getUser
                    next()
                } else {
                    throw { message: 'Auth failed' }
                }

            } else {
                throw { message: 'Auth failed' }
            }
        } else {
            throw { message: 'Auth failed' }
        }
    } catch (error) {
        if (error.message === 'jwt expired') {
            res.status(401).json({status: 401, message: 'Token Expaired'})
        } else {
            res.status(401).json({status: 401, message: 'Authentication failed'})
        }
    }
}

module.exports = {
    authenticaton
}
